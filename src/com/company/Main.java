package com.company;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");

        Connection connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:xe", "system", "1234");

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from AQ$_QUEUES");

        while (resultSet.next()){
            System.out.println(resultSet.getString(2)+" "+resultSet.getString(3));
        }

        connection.close();

        System.out.println("done");
    }
}
